package cn.tedu.sp03.user.service;

import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.service.UserService;
import cn.tedu.web.util.JsonUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.List;
@Slf4j
@Service
/**
 * 只有添加了@RefreshScope 刷新了新的用户数据后,才能重新注入到这个对象
 */
@RefreshScope
public class UserServiceImpl implements UserService {
    @Value("${sp.user-service.users}")
    private String userJson;

    /**
     * 从demo数据中查询,
     * 如果demo数据中没有,返回写死的数据
     */
    @Override
    public User getUser(Integer id) {
        log.info("users json string : "+userJson);
        //userJson转成List<User>
        //利用匿名内部类的继承于法,写泛型类型参数<List<User>>
        List<User> list = JsonUtil.from(userJson, new TypeReference<List<User>>() {});
        for (User u : list) {
            if (u.getId().equals(id)) {
                return u;
            }
        }
        //如果没有找到上面的用户,则写死写一条
        return new User(id, "name-"+id, "pwd-"+id);
    }

    @Override
    public void addScore(Integer id, Integer score) {
        // 这里增加积分
        log.info("user "+id+" - 增加积分 "+score);
    }

}

