# Hystrix
容错和限流工具
- 容错 - 降级
- 限流 - 断路器
降级
调用后台服务出错(异常、崩溃、超时)，可以执行一段降级代码返回降级结果
降级结果可以是：
- 错误提示
- 缓存数据
- 根据应用的逻辑返回任意内容
zuul添加降级代码

1. 实现 FallbackProvider 接口
2. 添加注解 `@Component`

zuul的自动配置会在spring容器中自动发现降级类的实例，完成自动配置
Hystrix 超时
如果不添加 ribbon 重试，默认是1秒超时
如果有 ribbon 重试，hystrix 超时会自动设置为 ribbon 重试的最大超时时间
Hystrix 熔断、流量限制
当对后台服务访问流量增大，出现大量错误，
可以把链路断开，减轻后台服务压力
- 10秒20次请求 （必须首先满足）
- 50%错误，执行了降级



断路器打开后，会进入“半开”状态，尝试发送一次客户端调用，

- 调用成功，自动关闭断路器，恢复正常
- 调用失败，继续保持打开状态

监控工具，可以监控 hystrix 降级和熔断的情况，快速定位故障位置
使用 actuator 暴露监控日志数据
actuator 是 springboot 提供的项目日志监控工具，提供项目的多种日志信息
- 健康状态
- spring容器中所有的对象
- spring mvc 映射的所有路径
- 环境变量和参数
- jvm虚拟机的内存镜像
- ....
仪表盘(不用注册刀eureka 和配置网关)
搭建 hystrix dashboard
1. 新建模块： sp07-hystrix-dashboard
2. 配置依赖： hystrix dashboard
3. yml 配置
   port: 4001
   允许抓取的服务器列表: localhost
4. 启动类注解： `@EnableHystrixDashboard`
5. 访问 `http://localhost:4001/hystrix`