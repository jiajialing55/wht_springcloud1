package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

//调用注解配置:1.调用那个服务2.调用服务的那个路径3.想这个路径提交什么参数
@FeignClient(name = "user-service")
public interface UserClient {
    //获取用户
    @GetMapping("/{userId}")
    JsonResult<User> getUser(@PathVariable Integer userId);

    //增加积分

    /**
     * @RequestParam("score")
     * 在controller中可以省略 在feign调用接口中省略可能会有问题
     */
    @GetMapping("/{userId}/score")
    JsonResult<?> addScore(@PathVariable Integer userId,@RequestParam("score") Integer score);

}
