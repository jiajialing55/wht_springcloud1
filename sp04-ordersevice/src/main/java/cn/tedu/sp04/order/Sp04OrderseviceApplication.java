package cn.tedu.sp04.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class Sp04OrderseviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sp04OrderseviceApplication.class, args);
    }

}