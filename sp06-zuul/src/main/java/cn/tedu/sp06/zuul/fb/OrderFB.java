package cn.tedu.sp06.zuul.fb;

import cn.tedu.web.util.JsonResult;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 调用商品服务失败时,执行失败时,窒息感这里得降级代码,返回降级结果
 */
@Component
public class OrderFB implements FallbackProvider {
    /*
   对哪个后台服务调用失败，应用当前降级类
   返回值：
       "item-service"  只针对商品服务降级
       "*"             对所有后台服务，都应用当前降级类
       null            对所有后台服务，都应用当前降级类


   如果我调商品失败,那此时这个降级类就会启动,然后开始进行降级响应实现fallbackResponse这个方法
   返回Response对象 我们的响应数据就封装在里面(协议头 体 状态)
    */
    @Override
    public String getRoute() {
        return "order-service";
    }
    /*
      返回给客户端的降级相应数据,
      封装到 response 对象
       */


    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        return new ClientHttpResponse() {
            @Override
            public HttpStatus getStatusCode() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR;
            }

            @Override
            public int getRawStatusCode() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR.value();
            }

            @Override
            public String getStatusText() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
            }

            @Override
            public void close() {
                // 用来关闭下面的流
                // BAIS 内存数组流，不占用底层系统资源，不需要关闭

                // 文件流，占用系统文件资源
                // 网络流，占用网络资源
                // 数据库流，占用数据库资源
            }

            @Override
            public InputStream getBody() throws IOException {
                // JsonResult --> {code:500, msg:调用后台服务出错, data:null}
                String json = JsonResult
                        .err().code(500).msg("调用后台服务出错").toString();
                // 将 json 封装到 BAIS
                return new ByteArrayInputStream(json.getBytes("UTF-8"));
            }

            @Override
            public HttpHeaders getHeaders() {
               HttpHeaders h = new HttpHeaders();
               h.set("Content-Type","application/json;charset=UTF-8");
               return h;
            }
        };
    }
}
